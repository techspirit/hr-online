import { navigate } from 'gatsby'

export const appRedirect = () => {
  history.replaceState(null, document.title, location.href.split('?')[0])

  if (location.pathname == '/') {
    navigate('/app/info')
  }
}
