import { IChildImageSharp } from './image'

export interface IQuest {
  image: IChildImageSharp
  name: string
}

export interface IQuestEdges {
  node: IQuest
}
