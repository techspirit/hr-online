import styled from 'styled-components'

export const Wrapper = styled.div`
  padding: 20px;
  background-color: ${({ theme }) => theme.palette.body};
  box-shadow: ${({ theme }) => theme.shadow};
`
