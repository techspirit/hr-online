import { IChildImageSharp } from './image'

export interface IUser {
  image: IChildImageSharp
  id: string
  name: string
  role: string
}

export interface IUserEdges {
  node: IUser
}
