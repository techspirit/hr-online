# HR Online 👨🏻‍

![Logo](./static/assets/thumbnail.png)

## Project includes
- 💅 Styled Components 
- 🛠 TypeScript
- 🔦 SEO meta description
- 🇬🇧 i18next
