import { GatsbyImageFluidProps } from 'gatsby-image'

export interface IChildImageSharp {
  childImageSharp: GatsbyImageFluidProps
}
