import styled from 'styled-components'

export const Wrapper = styled.main`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const ImageWrapper = styled.div`
  position: relative;
  width: 250px;
  height: 250px;
  overflow: hidden;
`
