const path = require('path')

module.exports = [
  {
    path: '/',
    component: path.resolve(`src/containers/index.tsx`),
  },
  {
    path: '/app',
    component: path.resolve(`src/containers/index.tsx`),
  },
  {
    path: '/unauthorised',
    component: path.resolve(`src/containers/unauthorised.tsx`),
  },
  {
    path: '/404',
    component: path.resolve(`src/containers/404.tsx`),
  },
]
